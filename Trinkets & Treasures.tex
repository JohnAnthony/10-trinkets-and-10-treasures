% Copyright 2021 John Anthony
% Released under a CC-BY-NC 3.0 license
% https://creativecommons.org/licenses/by-nc/3.0/

\documentclass[a5paper]{article}
\usepackage[table]{xcolor}
\usepackage{sectsty}
\usepackage{incgraph,tikz}
\usepackage{helvet}
\usepackage[graphicx]{realboxes}
\usepackage{enumitem}
\usepackage[bottom=2cm]{geometry}
\usepackage{hyperref}
\usepackage[some]{background}

\hypersetup{
    colorlinks,
    citecolor=black,
    filecolor=black,
    linkcolor=black,
    urlcolor=black
}

\allsectionsfont{\centering}
\renewcommand{\familydefault}{\sfdefault}
\setlength{\parindent}{0em}
\setlength\columnsep{2em}
\pagenumbering{gobble}

\newcommand{\trinketentry}[6]{
	\clearpage
	\addcontentsline{toc}{subsection}{#1}
	\begin{center}
		\Large
		\textbf{#1} \\
		\normalsize
		\vspace{6pt}
		\textbf{Creator:} #2 | \textbf{Artist:} #3 \\
	\end{center}
	\vfill\null
	\begin{center}
		\includegraphics[keepaspectratio=true,height=28em,width=25em]{img/#4}
	\end{center}
	\vfill\null
	\begin{center} \textbf{Value:} {#5}gp \end{center}
	#6
}

\newcommand{\treasureentry}[5]{
	\clearpage
	\addcontentsline{toc}{subsection}{#1}
	\begin{center}
		\Large
		\textbf{#1} \\
		\normalsize
		\vspace{6pt}
		\textbf{Creator:} #2 | \textbf{Artist:} #3 \\
	\end{center}
	\vfill\null
	\begin{center}
		\includegraphics[keepaspectratio=true,height=28em,width=25em]{img/#4}
	\end{center}
	\vfill\null
	#5
}

\newcommand{\AC}[0]{\textbf{AC}}

\begin{document}

\begin{titlepage}
	\newgeometry{margin=0.25in,top=2cm}
	\begin{center}
		\huge
		\textbf{Trinkets \& Treasures}
		\vspace{5pt}
		\small

		\vspace{1em}
		A collection of strange items \\
		both magical and mundane

		\vfill
		\includegraphics[width=20em]{img/cover.png}
		\vfill

		\includegraphics[width=13em]{img/ose.png} \\
		\vspace{2em}
		\textbf{Editor:} John Anthony \\
		\textbf{Cover \& Additional Art:} Alex Fleetham \\
	\end{center}
	\restoregeometry
\end{titlepage}

\newpage
\vfill\null

\newpage
\pagenumbering{arabic}
\tableofcontents

\vfill

\subsection*{Legal}

Old-School Essentials is a trademark of Necrotic Gnome. The trademark and Old-School Essentials logo are used with permission of Necrotic Gnome, under license. \\

All other text (including \LaTeX\  source code) and art are licensed under a CC BY-NC 3.0 license. All items credit their original creator and artist individually.

\vfill

\begin{center}
	Version 1.0 \\
	\today
\end{center}

\clearpage
\thispagestyle{empty}
\addcontentsline{toc}{section}{Trinkets}
\vspace*{\fill}
\begin{center}
%\includegraphics[angle=180]{squiggle.png} \\
\Huge{Trinkets}
%\includegraphics[]{squiggle.png} \\
\end{center}
\vspace*{\fill}
\clearpage

\trinketentry{Fool's Crown}{Ashley Pearce}{Poppy Chapman}{fools-crown.jpg}{10}{%
	A crown made of pyrite and stained glass. Its colour is subtly red-orange, but by torch and lantern light is indistinguishable from gold. The ``gems'' are brittle and softer than their precious counterparts, and fail to shine with similar lustre. Dwarves who perform even a cursory inspection have a 2-in-6 chance of recognising this object for what it is.
}%

\trinketentry{Grill of the Northmen}{Ben Fleetham}{Alex Fleetham}{grill-of-the-northmen.jpg}{50}{%
	A set of silver teeth set in dark tan rubber, relatively well carved to replace the teeth of ageing/badly beaten warriors. For characters with no teeth, there is a 1-in-6 chance of the Grill fitting comfortably enough to wear for extended periods of time.
}%

\trinketentry{Grooming Glove of Gan}{John Anthony}{Poppy Chapman}{grooming-glove-of-gan.jpg}{120}{%
	The back of this glove is decorated in sheet copper to resemble a large cat's paw, and the palm is covered with blunt wooden spikes suitable for brushing out the shed fur of lions, tigers and other large cats.
}%

\trinketentry{Mysterious Armlet}{Margaret Murphy}{Poppy Chapman}{mysterious-armlet.jpg}{75}{%
	This iron arm ring is heavy and rusty from disuse and lack of maintenance. It has a mysterious aura about it, from the hypnotic weaves of overlapping knot work wrought around the edge to the strange marine figures detailed onto its surface, almost like men who live both on land and in the depths of the sea. Will only fetch 50gp without a good clean.
}%

\trinketentry{Nightingale Music Box}{Denis McCarthy}{Poppy Chapman}{nightingale-music-box.jpg}{280}{%
	A silvered music box of about palm size. When opened, the delicate mechanism within moves a tiny silver nightingale and plays soft and beautiful music. Must be handled carefully. The mechanism is easily damaged, and the skills to repair such an item are in scarce supply. Only worth 60gp if inoperable.
}%

\trinketentry{Sandals of Spider Silk}{Joseph Norris}{Poppy Chapman}{sandals-of-spider-silk.jpg}{100}{%
	These durable and comfortable sandals are woven from painstakingly gathered giant spider silk. They are light and breathe well, and the silk from which they are made ignores the tangling effects of spiders' webs.
}%

\trinketentry{Snuff Box}{R. P. Graham}{Poppy Chapman}{snuff-box.jpg}{25}{%
	This dark oaken box is decorated about its lid with gold latticework hoop patterns. Inside, it is ruined velvet and carries the lingering smell of fruity spiced tobacco.
}%

\trinketentry{Temple Lamplighter/Candle Snuffer}{Thomas Skidmore}{Poppy Chapman}{candle-snuffer.jpg}{125}{%
	The decorative shaft of this 8' long brass candle snuffer displays oak and ivy entwined along its length. The bell itself is similarly of brass, shaped in the form of a small bird in flight. The bird carries an acorn, which serves as the snuffer. The piece is further decorated with bands of malachite inset for the handle and carved malachite set stones for the bird's eyes.
}%

\trinketentry{Winter Boots}{Red Dice Diaries}{Alex Fleetham}{winter-boots.jpg}{80}{%
	These traveller's boots are made of fine skins and furs that keep the wearer's feet at a constantly comfortable temperature. The boots are water resistant but, if fully submerged, will cease to function until dried out.
}%

\trinketentry{Worsted Tapestry}{Stephen Green}{Poppy Chapman}{worsted-tapestry.jpg}{275}{%
	This tapestry is crafted from cloth which is lightweight but coarse. It depicts first the migration of the Worsted noble family by sea, then their establishment as sheep farmers, and finally the creation of wool and cloth. It appears to be part historical catalogue and part advertisement, and bears the trade mark ``Worsted Cloth''.
}%

\clearpage
\thispagestyle{empty}
\vspace*{\fill}
\backgroundsetup{scale=1, opacity=1, angle=0, contents={
	\includegraphics[width=\paperwidth]{img/fill}
}}
\BgThispage

\clearpage
\thispagestyle{empty}
\addcontentsline{toc}{section}{Treasures}
\vspace*{\fill}
\begin{center}
%\includegraphics[angle=180]{squiggle.png} \\
\Huge{Treasures}
%\includegraphics[]{squiggle.png} \\
\end{center}
\vspace*{\fill}
\clearpage

\treasureentry{Bell of Rousing}{Ben Fleetham}{Alex Fleetham}{bell-of-rousing.jpg}{%
	A metal hand bell -- similar to the type used in schools -- that when rung removes the effects of sleep, fear and paralysis within 30'. Each time it's rung, it has a 1-in-6 chance of shattering rather than having the intended effect.
}%

\treasureentry{Collar of Creeps}{Joseph Norris}{Stacey Sanderson}{collar-of-creeps.png}{%
	This snakeskin collar will alert the wearer to any creature attempting to attack or pickpocket them by constricting suddenly. The wearer must save vs. paralysis or be stuck gasping and choking for a round when the collar constricts.
}%

\treasureentry{Flask of Faerie Dust}{Gordon Richards}{Alex Fleetham}{faerie-dust.jpg}{%
	A tube-like vial of light green dust tied with a pink bow. When blown or sprinkled upon a pile of loot, dust that comes into contact with magic items will glow purple for a few seconds before disappearing. There is enough powder in a flask for d6 uses.
}%

\treasureentry{Flockfear Feathers}{John Anthony}{Stacey Sanderson}{flockfear-feathers.jpg}{%
	A bundle of d6 jet black feathers which have the texture of coal. When burnt, will cause all birds within 120’ to save vs. spells or flee. Even upon a successful save vs. spells, birds may not approach to within 30' unless cornered or threatened. The effect lasts one hour, or until the flame is extinguished.
}%

\treasureentry{Glorious Hand}{Margeret Murphy}{Stacey Sanderson}{glorious-hand.png}{%
	A mummified hand mounted into a copper base like a gruesome candelabra. When a wick which protrudes from the index finger is lit, it will burn for a round before functioning as a sleep spell for the next five rounds. Each time a round passes beyond the first, a single digit will curl into the hand until the index finger closes in last, extinguishing the light and ending the spell.
}%

\treasureentry{Kelp Robes}{Stephen Green}{Alex Fleetham}{kelp-robes.jpg}{%
	These robes are made from semi-living seaweed, which becomes dormant when out of the sea or dry. When wet or under the sea the robes become tough, and even move and shift slightly to protect the wearer.
	When dry the robes offer no more protection than normal robes. When wet, the robes grant the wearer \AC 7 [12]. The robes have no effect if worn along with armour. If the robes are damaged for any reason they can only be repaired by submerging in the sea overnight.
}%

\treasureentry{Matching Pipe}{Erin D. Smale}{Stacey Sanderson}{matching-pipe.jpg}{%
	The so-called Matching Pipe can suggest how well the smoker might get along with an encountered individual. If the alignment of the smoker and the individual match, the pipe's smoke blows forth normally. If alignments differ, the smoke rises as weak and thin wisps. The smoker must engage in conversation with the individual for a minimum of one round, and the pipe functions but once per day.
}%

\treasureentry{Mimikey}{Ben Coulthard}{Alex Fleetham}{mimikey.png}{%
	A key-like rod of brittle black stone that sprouts bits (the elements of a key which cause it to fit a lock) when placed in a lock. It has a 1-in-6 chance of opening any mundane lock, and when used to successfully open a lock has a 2-in-6 chance of breaking.
}%

\treasureentry{Nouveau Spectacles}{Red Dice Diaries}{Poppy Chapman}{nouveau-spectacles.jpg}{%
	When worn these spectacles cause inanimate objects to appear as they would when they were first crafted. Any signs of age, later additions or graffiti are removed. For example: someone looking at a statue, hundreds of years old that is missing its arms, covered in moss and goblin graffiti, would see the statue clean and whole as it was when first installed.
	To see clearly through the spectacles requires some concentration, and is known to produce headaches, nosebleeds and worse on repeated use within a single day.
}%

\treasureentry{Onyx Lens Lantern}{Shawn McCarthy}{Stacey Sanderson}{onyx-lens-lantern.jpg}{%
	This item appears to be a common lantern with hinged door and optional hood, although where there should be panes of glass about the wick there are instead sheets of opaque onyx. When filled with oil and lit, it projects darkness instead of light (as the spell Darkness) appropriate for the shape of the lantern - hooded or unhooded.
	The effect is most pronounced where illumination is already dim; the lantern’s effect is much weaker than the Darkness spell, and is easily overpowered by direct sunlight or bright sources of illumination (such as torches or other lanterns).
}%

\end{document}
