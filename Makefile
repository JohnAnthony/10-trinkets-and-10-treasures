.phony: tidy

all: \
	Trinkets\ &\ Treasures.pdf \
	tidy

tidy:
	rm -f build/*.{log,aux,toc}

Trinkets\ &\ Treasures.pdf: Trinkets\ &\ Treasures.tex img/*
	pdflatex --halt-on-error "Trinkets & Treasures.tex"
	pdflatex --halt-on-error "Trinkets & Treasures.tex"
